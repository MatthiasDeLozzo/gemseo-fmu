# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# Contributors:
#    INITIAL AUTHORS - initial API and implementation and/or initial documentation
#        :author: Jorge CAMACHO CASERO
#    OTHER AUTHORS   - MACROSCOPIC CHANGES
"""
Create a discipline for the Sellar problem from a FMU file and computes its jacobians
==========================================
"""
from gemseo_fmu.use_cases.disciplines.sellar.sellar_1 import FMUSellar1
from gemseo_fmu.use_cases.disciplines.sellar.sellar_2 import FMUSellar2
from gemseo_fmu.use_cases.disciplines.sellar.sellar_system import FMUSellarSystem
from gemseo_fmu.use_cases.fmus import get_fmu_file_path

# %%
# Step 1: create the disciplines (with jacobians specific to the Sellar problem)
# In this example we take a FMU files directly from the FMU gallery
sellar_1 = FMUSellar1(get_fmu_file_path("Sellar1", "sellar"), kind="CS")
sellar_2 = FMUSellar2(get_fmu_file_path("Sellar2", "sellar"), kind="CS")
sellar_system = FMUSellarSystem(get_fmu_file_path("SellarSystem"), kind="CS")
disciplines = [sellar_1, sellar_2, sellar_system]

# %%
# Step 2: execute the disciplines
sellar_1.execute()
sellar_2.execute()
sellar_system.execute()

# %%
# Step 3: compute the jacobians
sellar_1.linearize(force_all=True)
sellar_2.linearize(force_all=True)
sellar_system.linearize(force_all=True)

# %%
# Step 4: access the jacobians
print(dict(sellar_1.jac))
print(dict(sellar_2.jac))
print(dict(sellar_system.jac))
