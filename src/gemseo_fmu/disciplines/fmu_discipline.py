# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""A discipline wrapping a Functional Mockup Unit (FMU) model."""
from __future__ import annotations

import logging
import shutil
from enum import auto
from pathlib import Path
from typing import Any
from typing import Mapping

from fmpy import extract
from fmpy import instantiate_fmu
from fmpy import read_model_description
from fmpy import simulate_fmu
from fmpy.util import fmu_info
from gemseo.core.discipline import MDODiscipline
from gemseo.core.grammars.simple_grammar import SimpleGrammar
from numpy import array
from numpy import ndarray
from numpy.typing import NDArray
from strenum import StrEnum

LOGGER = logging.getLogger(__name__)


class FMUDiscipline(MDODiscipline):
    """A discipline wrapping a Functional Mockup Unit (FMU) model.

    This discipline relies on `FMPy <https://github.com/CATIA-Systems/FMPy>`__.
    """

    class __StrEnum(StrEnum):
        """String constants."""

        step = auto()
        time = auto()
        timeline = auto()
        initial_time = auto()
        final_time = auto()
        restart = auto()
        CoSimulation = auto()
        ModelExchange = auto()
        simulation_time = auto()

    def __init__(
        self,
        fmu_file_path: str | Path,
        input_names: list[str],
        output_names: list[str],
        initial_time: float = 0.0,
        final_time: float = 1.0,
        time_step: float = 1.0,
        restart: bool = True,
        do_step: bool = False,
        name: str = "",
        use_co_simulation: bool = True,
        solver_name: str = "CVode",
        **pre_instantiation_parameters: Any,
    ) -> None:
        """
        Args:
            fmu_file_path: The path to the FMU model file.
            input_names: The names of the FMU model inputs.
            output_names: The names of the FMU model outputs.
            initial_time: The initial time of the simulation.
            final_time: The final time of the simulation.
                If ``0.`` and ``do_step`` is ``False``,
                either ``simulation_time`` or ``final_time`` must be passed
                to :meth:`.execute`.
            time_step: The time step of the simulation.
                If ``0.``, it is computed by the wrapped library ``fmpy``.
            restart: Whether to restart the model at ``initial_time``
                before executing it.
            do_step: Whether to simulate the model over only one ``time_step``
                when calling :meth:`.execute`.
                Otherwise, simulate the model from ``initial_time`` to ``final_time``.
            use_co_simulation: Whether to use the co-simulation FMI type.
                Otherwise, use model-exchange FMI type.
                When ``do_step`` is ``True``, the co-simulation FMI type is required.
            solver_name: The name of the solver to simulate a model-exchange model.
            **pre_instantiation_parameters: The parameters to be passed
                to :meth:`._pre_instantiate`.
        """  # noqa: D205 D212 D415
        # The path to the .fmu file, which is a ZIP archive.
        self.__fmu_file_path = fmu_file_path

        # The path to unzipped archive.
        self.__fmu_model_dir_path = extract(str(fmu_file_path))

        # The description of the FMU model, read from the XML file in the archive.
        self.__fmu_model_description = read_model_description(self.__fmu_model_dir_path)

        # The type of FMI.
        self.__fmu_model_type = (
            self.__StrEnum.CoSimulation
            if use_co_simulation
            else self.__StrEnum.ModelExchange
        )
        if do_step and not use_co_simulation:
            LOGGER.warning(
                "The FMUDiscipline requires a co-simulation model when do_step is True."
            )
            self.__fmu_model_type = self.__StrEnum.CoSimulation

        # The FMU model.
        self.__fmu_model = instantiate_fmu(
            self.__fmu_model_dir_path,
            self.__fmu_model_description,
            fmi_type=self.__fmu_model_type,
        )

        # The value references related to the variables names.
        self.__names_to_references = {
            variable.name: variable.valueReference
            for variable in self.__fmu_model_description.modelVariables
        }

        # The names of the FMU model outputs of interest.
        self.__fmu_model_output_names = output_names

        # The different times of interest.
        self.__current_time = self.__initial_time = initial_time
        self.__final_time = final_time
        self.__time_step = time_step
        if final_time == 0.0:
            final_time = initial_time

        # Initialize the FMU model.
        pre_instantiation_parameters = pre_instantiation_parameters or {}
        self._pre_instantiate(**pre_instantiation_parameters)
        self.__fmu_model.setupExperiment(startTime=self.__initial_time)
        self.__fmu_model.enterInitializationMode()
        self.__fmu_model.exitInitializationMode()

        # Store the initial values of the discipline outputs,
        # namely model outputs and time.
        self.__initial_values = {
            output_name: array(
                self.__fmu_model.getReal([self.__names_to_references[output_name]])
            )
            for output_name in output_names
        }
        self.__initial_values[self.__StrEnum.time.name] = array([self.__initial_time])
        self.__initial_values.update(
            {
                input_name: array(
                    self.__fmu_model.getReal([self.__names_to_references[input_name]])
                )
                for input_name in input_names
            }
        )

        # Whether to execute step by step.
        self.__do_step = do_step

        # The numerical solver to simulate the FMU model
        self.__solver_name = solver_name

        # Define the grammar of simulation settings.
        self.__simulation_grammar = SimpleGrammar("SimulationSettings")
        self.__simulation_grammar.update(
            [
                self.__StrEnum.restart.name,
                self.__StrEnum.timeline.name,
            ]
        )
        if not do_step:
            # In do_step mode,
            # an execution cannot simulate any simulation time but a time step.
            self.__simulation_grammar.update([self.__StrEnum.simulation_time.name])

        # Define the default values of the simulation settings.
        self.__simulation_settings = {}
        self.__default_simulation_settings = {
            self.__StrEnum.restart.name: restart,
            self.__StrEnum.timeline.name: array([self.__initial_time]),
        }
        if not do_step:
            self.__default_simulation_settings[self.__StrEnum.simulation_time.name] = (
                final_time - initial_time
            )

        # Make the object a MDODiscipline.
        super().__init__(
            name=name
            or None
            or self.__fmu_model_description.modelName
            or self.__class__.__name__,
            cache_type=None,
        )

        # Define the grammars of the model inputs and model outputs.
        self.input_grammar.update(input_names)
        self.output_grammar.update([self.__StrEnum.time.name] + output_names)

        # Store the initial output values in the local data.
        self.local_data.update(self.__initial_values)

    @property
    def initial_values(self) -> dict[str, NDArray[float]]:
        """The initial input, output and time values."""
        return self.__initial_values

    def __str__(self) -> str:
        return super.__str__(self) + "\n" + fmu_info(filename=self.__fmu_file_path)

    @property
    def _current_time(self) -> float:
        return self.__current_time

    @_current_time.setter
    def _current_time(self, current_time: float) -> None:
        """Set the current time.

        Args:
            current_time: The current time.

        Raises:
            ValueError: When the current time is greater than the final time.
        """
        if current_time > self.__final_time:
            raise ValueError(
                f"The current time ({current_time}) is greater "
                f"than the final time ({self.__final_time})."
            )

        self.__current_time = current_time

    def _pre_instantiate(self, **kwargs: Any) -> None:
        """Do different things before initializing the FMU model."""
        pass

    def execute(
        self, input_data: Mapping[str, ndarray] | None = None, **simulation_settings
    ):
        """
        Args:
            input_data: The values of the input variables of the FMU model.
            **simulation_settings: The simulation settings.
        """  # noqa: D205 D212 D415
        self.__simulation_settings = self.__default_simulation_settings.copy()
        self.__simulation_settings.update(simulation_settings)
        super().execute(input_data)

    def _run(self) -> None:
        input_data = self.get_input_data()
        if self.__simulation_settings[self.__StrEnum.restart.name]:
            self._current_time = self.__initial_time
            self.__fmu_model.reset()
            if self.__do_step:
                self.__fmu_model.enterInitializationMode()
                self.__fmu_model.exitInitializationMode()

        if self._current_time == self.__final_time:
            raise ValueError(
                "The discipline cannot be executed as its current time is final time."
            )

        if self.__do_step:
            self.__simulate_one_time_step(input_data)
        else:
            self.__simulate_to_final_time(input_data)

    def __del__(self) -> None:
        if self.__do_step:
            self.__fmu_model.terminate()
        self.__fmu_model.freeInstance()
        shutil.rmtree(self.__fmu_model_dir_path, ignore_errors=True)

    def __simulate_one_time_step(
        self, input_data: Mapping[str, NDArray[float]]
    ) -> None:
        """Simulate the FMU model during a single time step.

        Args:
            input_data: The values of the FMU model inputs.
        """
        for input_name, input_value in input_data.items():
            self.__fmu_model.setReal(
                [self.__names_to_references[input_name]], [input_value[0]]
            )

        self.__fmu_model.doStep(
            currentCommunicationPoint=self._current_time,
            communicationStepSize=self.__time_step,
        )

        final_time = self._current_time + self.__time_step
        for output_name in self.get_output_data_names():
            if output_name == self.__StrEnum.time.name:
                value = [self._change_time_unit(final_time)]
            else:
                value = self.__fmu_model.getReal(
                    [self.__names_to_references[output_name]]
                )

            self.local_data[output_name] = array(value)

        self._current_time = final_time

    def __simulate_to_final_time(
        self, input_data: Mapping[str, NDArray[float]]
    ) -> None:
        """Simulate the FMU model from the current time to the final time.

        Args:
            input_data: The values of the FMU model inputs.
        """
        time_series = []
        names = []
        start_values = {}
        for input_name, input_value in input_data.items():
            if input_value.size == 1:
                start_values[input_name] = input_value[0]
            else:
                time_series.append(input_value)
                names.append(input_name)

        initial_time = self._current_time
        final_time = (
            initial_time
            + self.__simulation_settings[self.__StrEnum.simulation_time.name]
        )
        if final_time > self.__final_time:
            final_time = self.__final_time
            LOGGER.warning("Stop the simulation at %s.", self.__final_time)

        if time_series:
            time_series = None
            # time_series = vstack([self.local_data[self.__TIME]] + time_series).T
            # time_series.dtype = [(name, np.float) for name in names]
        else:
            time_series = None

        def do_when_step_finished(time, recorder):
            fmu = recorder.fmu
            time_data = self.__simulation_settings[self.__StrEnum.timeline.name][::-1]
            for _index, time_datum in enumerate(time_data):
                if time > time_datum:
                    break

            for name in self.input_grammar.names:
                if name in self.local_data:
                    fmu.setReal(
                        [self.__names_to_references[name]],
                        [self.local_data[name][-(_index + 1)]],
                    )
            return True

        if initial_time == self.__initial_time:
            for name in self.input_grammar.names:
                if name in self.local_data:
                    self.__fmu_model.setReal(
                        [self.__names_to_references[name]], [self.local_data[name][0]]
                    )

        result = simulate_fmu(
            self.__fmu_model_dir_path,
            start_time=initial_time,
            stop_time=final_time,
            input=time_series,
            solver=self.__solver_name,
            output_interval=self.__time_step,
            step_size=self.__time_step,
            output=self.__fmu_model_output_names,
            fmu_instance=self.__fmu_model,
            model_description=self.__fmu_model_description,
            step_finished=do_when_step_finished,
        )
        for output_name in self.get_output_data_names():
            if output_name == self.__StrEnum.time.name:
                self.local_data[self.__StrEnum.time.name] = self._change_time_unit(
                    result[self.__StrEnum.time.name]
                )
            else:
                self.local_data[output_name] = self._post_process_output(
                    self.local_data[self.__StrEnum.time.name], result[output_name]
                )

        self._current_time = final_time

    def _post_process_output(
        self, time_data: NDArray[float], output_data: NDArray[float]
    ) -> NDArray[float]:
        """Post-process a time-indexed output.

        Args:
            time_data: The time values.
            output_data: The output values.

        Returns:
            The output values corresponding to the time ones.
        """
        return output_data

    @staticmethod
    def _change_time_unit(time: float | NDArray[float]) -> float | NDArray[float]:
        """Change the time unit.

        Args:
            time: The time with the original unit.

        Returns:
            The time with the new unit.
        """
        return time
