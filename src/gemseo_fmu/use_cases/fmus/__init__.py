# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
"""FMU use cases."""
from __future__ import annotations

from pathlib import Path
from sys import platform


def get_fmu_file_path(model_name: str, directory_name: str = ".") -> Path:
    """Return the file path of a FMU model.

    Args:
        model_name: The name of the FMU model.
        directory_name: The name of the directory containing the file.

    Returns:
        The file path of the FMU model.
    """
    os_dir = "linux" if platform.startswith("linux") else "win32"
    return Path(__file__).parent / os_dir / directory_name / f"{model_name}.fmu"
