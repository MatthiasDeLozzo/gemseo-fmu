# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# Contributors:
#    INITIAL AUTHORS - API and implementation and/or documentation
#        :author: Jorge Camacho
#    OTHER AUTHORS   - MACROSCOPIC CHANGES
"""The system discipline of the Sellar use case."""
from __future__ import annotations

from cmath import exp
from typing import Iterable

from numpy import array
from numpy import atleast_2d
from numpy import ndarray
from numpy import ones

from gemseo_fmu.fmu_discipline import FMUDiscipline

Y_1 = "y_1"
Y_2 = "y_2"
X_SHARED_1 = "x_shared_1"
X_SHARED_2 = "x_shared_2"
X_LOCAL = "x_local"
OBJ = "obj"
C_1 = "c_1"
C_2 = "c_2"
R_1 = "r_1"
R_2 = "r_2"


class FMUSellarSystem(FMUDiscipline):
    """The discipline to compute the objective and constraints of the Sellar problem."""

    @staticmethod
    def compute_obj(
        x_local: ndarray,
        x_shared: ndarray,
        y_1: ndarray,
        y_2: ndarray,
    ) -> float:
        """Evaluate the objective :math:`obj`.

        Args:
            x_local: The design variables local to the first discipline.
            x_shared: The shared design variables.
            y_1: The coupling variable coming from the first discipline.
            y_2: The coupling variable coming from the second discipline.

        Returns:
            The value of the objective :math:`obj`.
        """
        return x_local[0] ** 2 + x_shared + y_1[0] ** 2 + exp(-y_2[0])

    @staticmethod
    def compute_c_1(
        y_1: ndarray,
    ) -> float:
        """Evaluate the constraint :math:`c_1`.

        Args:
            y_1: The coupling variable coming from the first discipline.

        Returns:
            The value of the constraint :math:`c_1`.
        """
        return 3.16 - y_1[0] ** 2

    @staticmethod
    def compute_c_2(
        y_2: ndarray,
    ) -> float:
        """Evaluate the constraint :math:`c_2`.

        Args:
            y_2: The coupling variable coming from the second discipline.

        Returns:
            The value of the constraint :math:`c_2`.
        """
        return y_2[0] - 24.0

    def _compute_jacobian(
        self,
        inputs: Iterable[str] | None = None,
        outputs: Iterable[str] | None = None,
    ) -> None:
        self._init_jacobian(inputs, outputs, with_zeros=True)

        x_local, _, y_1, y_2 = self.get_inputs_by_name([X_LOCAL, X_SHARED_2, Y_1, Y_2])
        self.jac[C_1][Y_1] = atleast_2d(array([-2.0 * y_1]))
        self.jac[C_2][Y_2] = ones((1, 1))
        self.jac[OBJ][X_LOCAL] = atleast_2d(array([2.0 * x_local[0]]))
        self.jac[OBJ][X_SHARED_1] = atleast_2d(array([0.0]))
        self.jac[OBJ][X_SHARED_2] = atleast_2d(array([1.0]))
        self.jac[OBJ][Y_1] = atleast_2d(array([2.0 * y_1[0]]))
        self.jac[OBJ][Y_2] = atleast_2d(array([-exp(-y_2[0])]))
