# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
# Contributors:
#    INITIAL AUTHORS - API and implementation and/or documentation
#        :author: Jorge Camacho
#    OTHER AUTHORS   - MACROSCOPIC CHANGES
"""The second discipline of the Sellar use case."""
from __future__ import annotations

from typing import Iterable

from numpy import ndarray
from numpy import ones
from numpy import zeros

from gemseo_fmu.fmu_discipline import FMUDiscipline

Y_1 = "y_1"
Y_2 = "y_2"
X_SHARED_1 = "x_shared_1"
X_SHARED_2 = "x_shared_2"
X_LOCAL = "x_local"
OBJ = "obj"
C_1 = "c_1"
C_2 = "c_2"
R_1 = "r_1"
R_2 = "r_2"


class FMUSellar2(FMUDiscipline):
    """The discipline to compute the coupling variable :math:`y_2`."""

    def _compute_jacobian(
        self,
        inputs: Iterable[str] | None = None,
        outputs: Iterable[str] | None = None,
    ) -> None:
        self._init_jacobian(inputs, outputs, with_zeros=True)
        y_1 = self.get_inputs_by_name(Y_1)
        self.jac[Y_2] = {}
        self.jac[Y_2][X_LOCAL] = zeros((1, 1))
        self.jac[Y_2][X_SHARED_1] = ones((1, 1))
        self.jac[Y_2][X_SHARED_2] = ones((1, 1))
        if y_1[0] < 0.0:
            self.jac[Y_2][Y_1] = -ones((1, 1))
        elif y_1[0] == 0.0:
            self.jac[Y_2][Y_1] = zeros((1, 1))
        else:
            self.jac[Y_2][Y_1] = ones((1, 1))

    @staticmethod
    def compute_y_2(
        x_shared: ndarray,
        y_1: ndarray,
    ) -> float:
        """Evaluate the second coupling equation in functional form.

        Args:
            x_shared: The shared design variables.
            y_1: The coupling variable coming from the first discipline.

        Returns:
            The value of the coupling variable :math:`y_2`.
        """
        out = x_shared[0] + x_shared[1]
        if y_1[0].real == 0:
            y_2 = out
        elif y_1[0].real > 0:
            y_2 = y_1[0] + out
        else:
            y_2 = -y_1[0] + out
        return y_2
