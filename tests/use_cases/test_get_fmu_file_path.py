# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
from pathlib import Path
from unittest import mock

from gemseo_fmu.use_cases.fmus import get_fmu_file_path


@mock.patch("gemseo_fmu.use_cases.fmus.platform", "linux")
def test_get_fmu_file_path():
    """Check get_fmu_file_path()."""
    assert (
        get_fmu_file_path("foo")
        == Path(__file__).parent.parent.parent
        / "src"
        / "gemseo_fmu"
        / "use_cases"
        / "fmus"
        / "linux"
        / "foo.fmu"
    )
    assert (
        get_fmu_file_path("foo", "bar")
        == Path(__file__).parent.parent.parent
        / "src"
        / "gemseo_fmu"
        / "use_cases"
        / "fmus"
        / "linux"
        / "bar"
        / "foo.fmu"
    )
