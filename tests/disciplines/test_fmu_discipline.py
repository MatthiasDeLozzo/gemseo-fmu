# Copyright 2021 IRT Saint Exupéry, https://www.irt-saintexupery.com
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License version 3 as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
import re
from pathlib import Path

import pytest
from gemseo.utils.testing import compare_dict_of_arrays
from gemseo_fmu.disciplines.fmu_discipline import FMUDiscipline
from numpy import array
from numpy.testing import assert_almost_equal
from numpy.testing import assert_equal

FMU_PATH = Path(__file__).parent / "ramp.fmu"

INPUT_NAME = "ramp.height"
OUTPUT_NAME = "out"
TIME = "time"


@pytest.fixture(scope="module")
def standard_discipline() -> FMUDiscipline:
    """A standard FMU discipline."""
    discipline = FMUDiscipline(FMU_PATH, [INPUT_NAME], [OUTPUT_NAME])
    discipline.default_inputs[INPUT_NAME] = array([2.0])
    return discipline


@pytest.fixture
def custom_discipline_wo_restart() -> FMUDiscipline:
    """A FMU discipline with custom time settings and without restart."""
    discipline = FMUDiscipline(
        FMU_PATH,
        [INPUT_NAME],
        [OUTPUT_NAME],
        initial_time=0.0,
        final_time=0.6,
        time_step=0.2,
        restart=False,
    )
    discipline.default_inputs[INPUT_NAME] = array([2.0])
    return discipline


@pytest.fixture
def custom_discipline_w_restart() -> FMUDiscipline:
    """A FMU discipline with custom time settings and with restart."""
    discipline = FMUDiscipline(
        FMU_PATH,
        [INPUT_NAME],
        [OUTPUT_NAME],
        initial_time=0.0,
        final_time=0.6,
        time_step=0.2,
    )
    discipline.default_inputs[INPUT_NAME] = array([2.0])
    return discipline


@pytest.fixture
def custom_discipline_do_step() -> FMUDiscipline:
    """A FMU discipline with custom time settings, without restart and with do_step."""
    discipline = FMUDiscipline(
        FMU_PATH,
        [INPUT_NAME],
        [OUTPUT_NAME],
        initial_time=0.0,
        final_time=2.0,
        time_step=0.2,
        do_step=True,
        restart=False,
    )
    discipline.default_inputs[INPUT_NAME] = array([2.0])
    return discipline


@pytest.fixture
def custom_discipline_do_step_w_restart() -> FMUDiscipline:
    """A FMU discipline with custom time settings, with restart and with do_step."""
    discipline = FMUDiscipline(
        FMU_PATH,
        [INPUT_NAME],
        [OUTPUT_NAME],
        initial_time=0.0,
        final_time=2.0,
        time_step=0.2,
        do_step=True,
    )
    discipline.default_inputs[INPUT_NAME] = array([2.0])
    return discipline


def test_do_step(standard_discipline):
    """Check that the disciplines does not execute the FMU model step by step."""
    assert not standard_discipline._FMUDiscipline__do_step


def test_discipline_input_names(standard_discipline):
    """Check the names of the discipline inputs."""
    assert set(standard_discipline.get_input_data_names()) == {INPUT_NAME}


def test_discipline_output_names(standard_discipline):
    """Check the names of the discipline outputs."""
    assert set(standard_discipline.get_output_data_names()) == {"time", OUTPUT_NAME}


def test_str(standard_discipline):
    """Check that the string representation of a FMUDiscipline contain."""
    assert str(standard_discipline).startswith(
        "ramp_model\n"
        "   Inputs: ramp.height\n"
        "   Outputs: out, time\n\n"
        "Model Info"
    )


def test_default_inputs(standard_discipline):
    """Check the default inputs of the discipline."""
    default_inputs = standard_discipline.default_inputs
    assert set(default_inputs) == {INPUT_NAME}
    assert_equal(default_inputs[INPUT_NAME], array([2.0]))


def test_initial_values(custom_discipline_wo_restart, custom_discipline_do_step):
    """Check the initial values."""
    assert compare_dict_of_arrays(
        custom_discipline_wo_restart.initial_values,
        {"out": array([0.0]), "time": array([0.0]), "ramp.height": array([1.0])},
    )
    assert compare_dict_of_arrays(
        custom_discipline_do_step.initial_values,
        {"out": array([0.0]), "time": array([0.0]), "ramp.height": array([1.0])},
    )


@pytest.mark.parametrize("time", [1.0, 2.0, array([1.0]), array([1.0, 2.0])])
def test_change_time_unit(time):
    """Check that the default _change_time_unit() is the identity function."""
    assert_equal(FMUDiscipline._change_time_unit(time), time)


def test_postprocess_output(standard_discipline):
    """Check that the default output post-processing is the identity function."""
    assert_equal(
        standard_discipline._post_process_output(array([1.0]), array([2.0])),
        array([2.0]),
    )


def test_set_current_time(standard_discipline):
    """Check that a current time cannot be greater than the stop time."""
    with pytest.raises(
        ValueError,
        match=re.escape("The current time (2.0) is greater than the final time (1.0)."),
    ):
        standard_discipline._current_time = 2.0


def test_execute_without_do_step(custom_discipline_wo_restart):
    """Check the execution from start to final time, w/o custom values and w/o restart.

    Here we consider a discipline not using restart by default.
    """
    custom_discipline_wo_restart.execute()
    assert_almost_equal(
        custom_discipline_wo_restart.local_data[TIME], array([0.0, 0.2, 0.4, 0.6])
    )
    assert_almost_equal(
        custom_discipline_wo_restart.local_data[OUTPUT_NAME],
        array([0.0, 0.4, 0.8, 1.2]),
    )
    with pytest.raises(
        ValueError,
        match=re.escape(
            "The discipline cannot be executed as its current time is final time."
        ),
    ):
        custom_discipline_wo_restart.execute()


def test_execute_without_do_step_r(custom_discipline_w_restart):
    """Check execution from initial to final times, w/o custom values and w/o restart.

    Here we consider a discipline using restart by default.
    """
    time_data = array([0.0, 0.2, 0.4, 0.6])
    output_data = array([0.0, 0.4, 0.8, 1.2])
    custom_discipline_w_restart.execute()
    assert_almost_equal(custom_discipline_w_restart.local_data[TIME], time_data)
    assert_almost_equal(
        custom_discipline_w_restart.local_data[OUTPUT_NAME], output_data
    )
    custom_discipline_w_restart.execute()
    assert_almost_equal(custom_discipline_w_restart.local_data[TIME], time_data)
    assert_almost_equal(
        custom_discipline_w_restart.local_data[OUTPUT_NAME], output_data
    )


def test_execute_with_do_step(custom_discipline_do_step):
    """Check the execution step by step, w/o custom values and w/o restart.

    Here we consider a discipline not using restart by default.
    """
    # One step forward with the standard ramp.
    custom_discipline_do_step.execute()
    assert_almost_equal(custom_discipline_do_step.local_data[TIME], array([0.2]))
    assert_almost_equal(custom_discipline_do_step.local_data[OUTPUT_NAME], array([0.4]))

    # One step forward with the standard ramp.
    custom_discipline_do_step.execute()
    assert_almost_equal(custom_discipline_do_step.local_data[TIME], array([0.4]))
    assert_almost_equal(custom_discipline_do_step.local_data[OUTPUT_NAME], array([0.8]))

    # One step forward with a custom ramp.
    custom_discipline_do_step.execute({INPUT_NAME: array([1.0])})
    assert_almost_equal(custom_discipline_do_step.local_data[TIME], array([0.6]))
    assert_almost_equal(custom_discipline_do_step.local_data[OUTPUT_NAME], array([0.6]))

    # One step forward with the standard ramp.
    custom_discipline_do_step.execute()
    assert_almost_equal(custom_discipline_do_step.local_data[TIME], array([0.8]))
    assert_almost_equal(custom_discipline_do_step.local_data[OUTPUT_NAME], array([1.6]))

    # One step forward with the standard ramp after restart.
    custom_discipline_do_step.execute(restart=True)
    assert_almost_equal(custom_discipline_do_step.local_data[TIME], array([0.2]))
    assert_almost_equal(custom_discipline_do_step.local_data[OUTPUT_NAME], array([0.4]))

    # One step forward with a custom ramp.
    custom_discipline_do_step.execute({INPUT_NAME: array([1.0])})
    assert_almost_equal(custom_discipline_do_step.local_data[TIME], array([0.4]))
    assert_almost_equal(custom_discipline_do_step.local_data[OUTPUT_NAME], array([0.4]))


def test_execute_with_do_step_r(custom_discipline_do_step_w_restart):
    """Check the execution step by step, w/o custom values and w/o restart.

    Here we consider a discipline using restart by default.
    """
    # One step forward with the standard ramp.
    custom_discipline_do_step_w_restart.execute()
    assert_almost_equal(
        custom_discipline_do_step_w_restart.local_data[TIME], array([0.2])
    )
    assert_almost_equal(
        custom_discipline_do_step_w_restart.local_data[OUTPUT_NAME], array([0.4])
    )

    # One step forward with the standard ramp.
    custom_discipline_do_step_w_restart.execute()
    assert_almost_equal(
        custom_discipline_do_step_w_restart.local_data[TIME], array([0.2])
    )
    assert_almost_equal(
        custom_discipline_do_step_w_restart.local_data[OUTPUT_NAME], array([0.4])
    )

    # One step forward with a custom ramp.
    custom_discipline_do_step_w_restart.execute({INPUT_NAME: array([1.0])})
    assert_almost_equal(
        custom_discipline_do_step_w_restart.local_data[TIME], array([0.2])
    )
    assert_almost_equal(
        custom_discipline_do_step_w_restart.local_data[OUTPUT_NAME], array([0.2])
    )

    # One step forward with the standard ramp.
    custom_discipline_do_step_w_restart.execute()
    assert_almost_equal(
        custom_discipline_do_step_w_restart.local_data[TIME], array([0.2])
    )
    assert_almost_equal(
        custom_discipline_do_step_w_restart.local_data[OUTPUT_NAME], array([0.4])
    )

    # One step forward with the standard ramp without restart.
    custom_discipline_do_step_w_restart.execute(restart=False)
    assert_almost_equal(
        custom_discipline_do_step_w_restart.local_data[TIME], array([0.4])
    )
    assert_almost_equal(
        custom_discipline_do_step_w_restart.local_data[OUTPUT_NAME], array([0.8])
    )

    # One step forward with a custom ramp.
    custom_discipline_do_step_w_restart.execute({INPUT_NAME: array([1.0])})
    assert_almost_equal(
        custom_discipline_do_step_w_restart.local_data[TIME], array([0.2])
    )
    assert_almost_equal(
        custom_discipline_do_step_w_restart.local_data[OUTPUT_NAME], array([0.2])
    )
